<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'title',
        'excerpt',
        'content',
        'image',
        'category_id',
        'published_at'
    ];
    public function deleteImage()
    {
        Storage::delete($this->image);
    }
    public function category() {
        return $this->belongsTo(Category::class);
    }
}