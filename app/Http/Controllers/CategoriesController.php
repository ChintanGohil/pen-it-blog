<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', compact([
            'categories'
        ]));
    }

    public function create()
    {
        return view('categories.create');
    }
    public function store(Request $request)
    {
        // 1.validate data

        // 2.store the data in db 
        Category::create([
            'name'=>$request->name
        ]);

        // 3.session set something and then return a view

        session()->flash('success','category Added Successfully!');
        return redirect(route('categories.index'));
    }
    public function show(Category $category)
    {
        //
    }

    public function edit(Category $category)
    {
        return view('categories.edit',compact([
            'category'
        ]));
    }
    public function update(Request $request, Category $category)
    {
        $category->name = $request->name;
        $category->save();
        session()->flash('success','category updated successfully');
        return redirect(route('categories.index'));
    }

    public function destroy(Category $category)
    {
        $category->delete();
        session()->flash('success',"category deleted successfully");
        return redirect(route('categories.index'));
    }
}
