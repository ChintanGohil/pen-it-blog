<?php

namespace App\Http\Requests\Posts;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|unique:post,title,'.$this->post->id,
            'excerpt'=>'required',
            'content'=>'required',
            'image'=>'image|mimes:jpg,jpeg,gif,png|max:1024'
        ];
    }
}
